import React from "react";
import history from "../../history";
import api from '../API/api';
import {apiurlprefix} from '../../apiconfig';
import VirtualUrl from '../../VirtualGalleryConfig.json';
import Loader from '../container/loader';
import {isMobile} from "react-device-detect";

interface IFooter{
  toptag:[];
// topcategory:[];
  exblist:[];
  iframeurl:string;
  loading:boolean;
  windowWidth:any;
}

class Footer extends React.Component<{}, IFooter> {
  constructor(props: {}) {
    super(props);
    this.state = {
      toptag:[],
      // topcategory:[],
      exblist:[],
      iframeurl:'',
      loading:true,
      windowWidth: window.innerWidth
    }
  }
  handleResize = (e:Event) => {
    this.setState({ windowWidth: window.innerWidth });
   };
  async onload(){
    const toptag = await api.getTopTags();
    // const topcat = await api.getTopCategory();
    let res = await api.getAllExb();
    console.log(res.slice(0,3))
    this.setState({toptag:toptag,/*topcategory:topcat,*/exblist:res.slice(0,3),loading:false});
  }
  componentDidMount(){
    // let data = JSON.stringify(toptags());
    // let sample = JSON.parse(data);
    // console.log(sample[0].tag_id);
    this.onload();
    window.addEventListener("resize", this.handleResize);
    
    
    // axios
    //     .get(`${apiurlprefix}/toptag`)
    //     .then((res) => {this.setState({toptag:res.data})});
    // axios
    //    .get(`${apiurlprefix}/topcategory`)
    //    .then((res) => {this.setState({topcategory:res.data})});
  }
  componentWillUnmount() {
    window.addEventListener("resize", this.handleResize);
   } 
  searchNavigate(tagname:string){
    history.push(`/search/${tagname}/0`);
  }
  dateformat(date:string){
    let dayremove=new Date(date).toDateString().split(' ').slice(1);
    return dayremove[1]+' '+dayremove[0]+' '+dayremove[2];
  }
  iframeload(userid :number,exbid:number){
    let logonUserId = 0;
    if(localStorage.getItem('userid') !== null){
      logonUserId = parseInt(localStorage.getItem('userid') || '');
    }
    this.setState({iframeurl:`${apiurlprefix}/`+
    VirtualUrl.virtualGalleryUrl +
    `?userid=${userid}&exhibitionid=${exbid}&loggeduserid=${logonUserId}`})
  }
  InfoMsg(){
    alert("For the best experience please use a laptop or fesktop computer. The interactive viewer is not supported on mobile devices yet, but stay tuned for the mobile app.");
  }
  render() {
    const { windowWidth } = this.state; 
    return (
    <footer className="dark-wrapper inverse-text">
    <div className="container inner pt-60 pb-60">
      {this.state.loading?<Loader/>:
      <div className="row">
        <div className="col-md-12 col-lg-4 text-center text-lg-left">
          <div className="widget">
            <img
              src="/assets/images/ecanvasprologo.png.png"
              className="header_logo"
              alt="logo"
            />
            <div className="space20"></div>
            <p>
              We are a photography studio specializing in wedding, jewelry,
              birthday and fashion photography, based in New York.
            </p>
            <div className="space20"></div>
            <p>© 2018 E-Canvas. All rights reserved.</p>
            <ul className="social social-bg social-s social-footer">
              <li>
                <a href=".">
                  <i className="fa fa-twitter"></i>
                </a>
              </li>
              <li>
                <a href=".">
                  <i className="fa fa-facebook-f"></i>
                </a>
              </li>
              <li>
                <a href=".">
                  <i className="fa fa-pinterest"></i>
                </a>
              </li>
              <li>
                <a href=".">
                  <i className="fa fa-vimeo"></i>
                </a>
              </li>
              <li>
                <a href=".">
                  <i className="fa fa-instagram"></i>
                </a>
              </li>
            </ul>
          </div>
        </div>
        <div className="col-md-6 col-lg-4">
          <div className="widget">
            <h3 className="widget-title">Tags</h3>
            <ul className="list-unstyled tag-list">
              {this.state.toptag.map((item:any)=>(
                <li key={item.tag_id} onClick={()=>this.searchNavigate(item.tag_name)}>
                <span className="btn btn-full-rounded">
                  {item.tag_name}
                </span>
              </li>
              ))}
            </ul>
          </div>
        </div>
      </div>}
    </div>
  </footer>
  );
  }
}

export default Footer;
