import React from 'react';

function Footer2() {
  return (
    <footer className="dark-wrapper inverse-text mulifont">
          <div className="sub-footer">
            <div className="container inner text-center">
              <img
                src="/assets/images/ecanvas_logo3.png"
                className="footer_logo"
                alt=""
              />
              <div className="space25"></div>
              <p>© 2018 E-Canvas. All rights reserved.</p>
              <div className="space30"></div>
              <ul className="social social-bg social-s">
                <li>
                  <a href=".">
                    <i className="fa fa-twitter"></i>
                  </a>
                </li>
                <li>
                  <a href=".">
                    <i className="fa fa-facebook-f"></i>
                  </a>
                </li>
                <li>
                  <a href=".">
                    <i className="fa fa-pinterest"></i>
                  </a>
                </li>
                <li>
                  <a href=".">
                    <i className="fa fa-vimeo"></i>
                  </a>
                </li>
                <li>
                  <a href=".">
                    <i className="fa fa-instagram"></i>
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </footer>
  );
}

export default Footer2;
