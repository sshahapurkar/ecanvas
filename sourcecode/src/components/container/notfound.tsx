import React, { Component } from 'react';
import Link from 'react-router';
import history from '../../history';
export default class NotFound extends React.Component<any,any> {
    render() {
      return (
        <div>
            <img src='/assets/images/404.jpg'  />
            <p style={{textAlign:"center",margin:'30px'}}>
              <button className='btn btn-full-rounded' onClick={()=>history.push('/')}>Go to Home</button>
            </p>
          </div>
      );
    }
  }