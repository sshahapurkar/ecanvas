import React, { Component } from "react";
import { Sliderindex } from "../Slider/slider";
import { Adminslider } from "../AdminSlider/adminslider";
import Footer from "../Footer/footer";
import { Flipper, Flipped } from "react-flip-toolkit";
import anime from "animejs";
import api from "../API/api";
import { apiurlprefix } from "../../apiconfig";
import VirtualUrl from "../../VirtualGalleryConfig.json";
import Loader from './loader';
import {isMobile} from "react-device-detect";

interface IHome {
  catagoryList: string[];
  filter: string | undefined;
  selectView?: string;
  results: any;
  iframeurl: string;
  loading: boolean;
  windowWidth:any;
}

const onElementAppear = (el: any, i: any) => {
  return anime({
    targets: el,
    opacity: 1,
    delay: i * 10,
    width: "100%",
    easing: "easeOutSine",
  });
};

const onElementExit = (el: any, i: number, onComplete: any) => {
  return anime({
    targets: el,
    opacity: 0,
    delay: i * 10,
    easing: "easeOutSine",
    complete: onComplete,
  });
};

class Home extends Component<{}, IHome> {
  constructor(props: {}) {
    super(props);
    // const cat: string[] = ExhibitionData.map((x) => x.category).filter(
    //   (curr, i, arr) => arr.indexOf(curr) === i
    // ) as string[];
    // console.log(cat);

    this.state = {
      catagoryList: [],
      filter: undefined,
      selectView: "",
      results: [],
      iframeurl: "",
      loading: true,
      windowWidth: window.innerWidth
    };
  }

   handleResize = (e:Event) => {
    this.setState({ windowWidth: window.innerWidth });
   };

   async onload() {
    let res = await api.getAllExb();
    const cat: string[] = res
      .map((x: any) => x.categories_name)
      .slice(0, 9)
      .filter(
        (curr: any, i: any, arr: any) => arr.indexOf(curr) === i
      ) as string[];
    this.setState({ catagoryList: cat, results: res, loading: false });

    // this.setState({exblist:res,loading:false});
  }
  
  componentDidMount() {
    this.onload();
    window.addEventListener("resize", this.handleResize);
  }
  componentWillUnmount() {
    window.addEventListener("resize", this.handleResize);
   } 
  iframeload(userid: number, exbid: number) {
    if(window.innerWidth > 500){
    let logonUserId = 0;
    if (localStorage.getItem("userid") !== null) {
      logonUserId = parseInt(localStorage.getItem("userid") || "");
    }
    this.setState({
      iframeurl:
        `${apiurlprefix}/` +
        VirtualUrl.virtualGalleryUrl +
        `?userid=${userid}&exhibitionid=${exbid}&loggeduserid=${logonUserId}`,
    });
  }else{
    alert("For the best experience please use a laptop or fesktop computer. The interactive viewer is not supported on mobile devices yet, but stay tuned for the mobile app.");
  }
  }

  render() {
    const { catagoryList } = this.state;
    const { windowWidth } = this.state; 

    const selectCategory = (element: string) => {
      this.setState({
        filter: element,
      });
    };
    return (
      <div className="home">
        <div className="wrapper light-wrapper">
          <div className="container inner pt-60">
            <h1 className="heading text-center">
              Electronic platform for art enthusiasts
            </h1>
            <div className="space2"></div>
            <Sliderindex />
          </div>
        </div>
        <Flipper flipKey={this.state.filter}>
          <div className="wrapper gray-wrapper">
            <div className="container inner">
              <h2 className="section-title text-center">
                ALL TYPE OF IMAGES
              </h2>
              <div className="space2"></div>
              <div
                id="cube-grid-full-filter"
                className="cbp-filter-container text-center"
              >
                <div
                  onClick={() => selectCategory("")}
                  className={`${
                    !this.state.filter ? "cbp-filter-item-active" : ""
                  } cbp-filter-item`}
                >
                  All
                </div>
                {catagoryList.map((item) => (
                  <FilterPanel
                    catName={item}
                    key={item}
                    classes={`${
                      this.state.filter == item ? "cbp-filter-item-active" : ""
                    } cbp-filter-item`}
                    onSelect={selectCategory}
                  />
                ))}
              </div>
              <div className="clearfix"></div>
              <div className="space20"></div>
              {this.state.loading?<Loader/>:
              <div id="cube-grid-full" className="row">
                {this.state.results
                  .sort()
                  .slice(0, 9)
                  .filter((d: any) => {
                    if (!this.state.filter) {
                      return true;
                    }
                    return d.categories_name === this.state.filter;
                  })
                  .map((item: any, i: any) => {
                    return (
                      <Flipped
                        //flipId={`item-${item.category}=${i}`}
                       flipId={`item-${item.virtualexhibition_avatar}`}
                        onAppear={onElementAppear}
                        onStart={onElementAppear}
                        onExit={onElementExit}
                        key={`item-${item.categories_name}=${i}`}
                      >
                        <div className="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-6 exb_gal">
                          <figure className="overlay-info hovered">
                          {(!isMobile)
                              ? <button
                              data-toggle="modal"
                              data-target="#myModal"
                              data-backdrop="static"
                              data-keyboard="false"
                              className="button_slider"
                              onClick={() =>
                                this.iframeload(
                                  item.user_id,
                                  item.virtualexhibition_id
                                )
                              }
                            >
                              <img
                                src={
                                  `${apiurlprefix}/` +
                                  item.virtualexhibition_avatar
                                }
                              />
                            </button>
                              : <button
                              
                              className="button_slider"
                              onClick={() =>
                                this.iframeload(
                                  item.user_id,
                                  item.virtualexhibition_id
                                )
                              }
                            >
                              <img
                                src={
                                  `${apiurlprefix}/` +
                                  item.virtualexhibition_avatar
                                }
                              />
                            </button>
                            }
                            
                            <figcaption className="d-flex">
                              <div className="align-self-center mx-auto">
                                <h4 className="mb-0">
                                  {item.virtualexhibition_theme}
                                </h4>
                              </div>
                            </figcaption>
                          </figure>
                        </div>
                      </Flipped>
                    );
                  })}
              </div>}
            </div>
          </div>
        </Flipper>
        <div className="modal" id="myModal" tabIndex={-1}>
          <div className="modal-dialog">
            <div className="modal-content">
              <iframe
                src={this.state.iframeurl}
                id="popup1"
                title="popup"
                className="modal-body"
                style={{ width: "100%", minHeight: "550px" }}
              ></iframe>
              <div className="modal-footer">
                <button
                  type="button"
                  className="btn btn-full-rounded"
                  data-dismiss="modal"
                >
                  Close
                </button>
              </div>
            </div>
          </div>
        </div>
        <div className="wrapper gray-wrapper">
          <div className="container inner pt-60">
            <div className="space20"></div>
            <Adminslider />
          </div>
        </div>
        <Footer />
      </div>
    );
  }
}

interface IFilterPanelProps {
  onSelect: (cat: string) => void;
  catName: string;
  classes: string;
}
const FilterPanel = (props: IFilterPanelProps) => {
  return (
    <div
      className={props.classes}
      onClick={() => props.onSelect(props.catName)}
    >
      {props.catName}
    </div>
  );
};

export default Home;
