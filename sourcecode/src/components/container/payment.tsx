import React, { Component } from "react";
import Footer2 from "../Footer/footer2";
import { Link } from "react-router-dom";

interface IPayment {}

class Payment extends Component<IPayment, {}> {
  constructor(props: IPayment) {
    super(props);
  }

  render() {
    return (
      <div>
        <div className="wrapper gray-wrapper payment_container">
          <div className="container inner pt-60">
            <div className="row">
              <div className="col-lg-7 col-md-7">
                <h1 className="heading">Buy Gallery</h1>
                <div className="row mulifont">
                  <div className="col-lg-12">
                    <label htmlFor="">
                      Please review the details of your gallery below. select
                      your payment period then continue.
                    </label>
                  </div>
                  <div className="space20"></div>
                  <div className="col-lg-2 col-md-3 col-sm-3 col-2">
                    <label htmlFor="">Gallery</label>
                  </div>
                  <div className="col-lg-10 col-md-7 col-sm-7 col-10">
                    <label htmlFor="">Marble Gallery</label>
                  </div>
                  <div className="space20"></div>
                  <div className="col-lg-2 col-md-3 col-sm-3 col-2">
                    <label htmlFor="quantity" className="quality">
                      Quantity
                    </label>
                  </div>
                  <div className="col-lg-10 col-md-7 col-sm-7 col-10">
                    <input type="number" name="quantity" />
                  </div>
                  <div className="space20"></div>
                  <div className="col-lg-2 col-md-3 col-sm-3 col-2">
                    <label htmlFor="">Payment Period</label>
                  </div>
                  <div className="col-lg-10 col-md-9 col-sm-9 col-10">
                    <div className="row plan">
                      <div className="custom-control custom-radio custom-control-inline col-lg-3 col-md-4 col-sm-3 col-4">
                        <input
                          type="radio"
                          id="customRadioInline1"
                          name="customRadioInline1"
                          className="custom-control-input"
                        />
                        <label
                          className="custom-control-label"
                          htmlFor="customRadioInline1"
                        >
                          Monthly USD$25.00
                        </label>
                      </div>
                      <div className="custom-control custom-radio custom-control-inline col-lg-3 col-md-4 col-sm-3 col-4">
                        <input
                          type="radio"
                          id="customRadioInline2"
                          name="customRadioInline1"
                          className="custom-control-input"
                        />
                        <label
                          className="custom-control-label"
                          htmlFor="customRadioInline2"
                        >
                          Six Monthly USD$138.00
                        </label>
                      </div>
                      <div className="custom-control custom-radio custom-control-inline col-lg-3 col-md-4 col-sm-3 col-4">
                        <input
                          type="radio"
                          id="customRadioInline3"
                          name="customRadioInline1"
                          className="custom-control-input"
                        />
                        <label
                          className="custom-control-label"
                          htmlFor="customRadioInline3"
                        >
                          Annually USD$252.00
                        </label>
                      </div>
                    </div>
                  </div>
                </div>
                <hr />
                <h1 className="heading">Voucher Code</h1>
                <div className="row mulifont">
                  <div className="col-lg-12">
                    <label htmlFor="">
                      If you are an member of an Exhibbit partner site and hold
                      a voucher code please enter it below
                    </label>
                  </div>
                  <div className="space20"></div>
                  <div className="col-lg-12">
                    <input type="text" />
                    <button className="apply_btn btn btn-full-rounded">
                      Apply
                    </button>
                  </div>
                  <div className="space20"></div>
                </div>
              </div>
              <hr />
              <div className="col-lg-5 col-md-5">
                <h1 className="heading">Create Payment Method</h1>
                <div className="padding">
                  <div className="row mulifont">
                    <div className="col-lg-12">
                      <div className="card">
                        <div className="card-header">
                          <strong>
                            Please enter credit card details below to create
                            your payment method
                          </strong>
                        </div>
                        <div className="card-body">
                          <div className="row"></div>
                          <div className="row">
                            <div className="col-lg-12">
                              <div className="form-group">
                                <label htmlFor="ccnumber">
                                  Credit Card Number
                                </label>
                                <div className="input-group">
                                  <input
                                    className="form-control"
                                    type="text"
                                    placeholder="0000 0000 0000 0000"
                                  />
                                </div>
                              </div>
                            </div>
                          </div>
                          <div className="row">
                            <div className="form-group col-lg-4 col-md-6 col-sm-4 col-4">
                              <label htmlFor="ccmonth">Month</label>
                              <div className="custom-select-wrapper">
                                <select
                                  className="form-control custom-select"
                                  id="ccmonth"
                                >
                                  <option>1</option>
                                  <option>2</option>
                                  <option>3</option>
                                  <option>4</option>
                                  <option>5</option>
                                  <option>6</option>
                                  <option>7</option>
                                  <option>8</option>
                                  <option>9</option>
                                  <option>10</option>
                                  <option>11</option>
                                  <option>12</option>
                                </select>
                              </div>
                            </div>
                            <div className="form-group col-lg-4 col-md-6 col-sm-4 col-4">
                              <label htmlFor="ccyear">Year</label>
                              <div className="custom-select-wrapper">
                                <select
                                  className="form-control custom-select"
                                  id="ccyear"
                                >
                                  <option>2020</option>
                                  <option>2021</option>
                                  <option>2022</option>
                                  <option>2023</option>
                                  <option>2024</option>
                                  <option>2025</option>
                                </select>
                              </div>
                            </div>
                            <div className="col-lg-4 col-md-6 col-sm-4 col-4">
                              <div className="form-group">
                                <label htmlFor="cvv">CVC</label>
                                <input
                                  className="form-control"
                                  id="cvv"
                                  type="text"
                                  placeholder="123"
                                />
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="card-footer">
                          <Link to="/newexhibition">
                            <button
                              className="btn btn-lg float-right btn btn-full-rounded payment_btn"
                              type="submit"
                            >
                              Make Payment
                            </button>
                          </Link>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <Footer2 />
      </div>
    );
  }
}

export default Payment;
