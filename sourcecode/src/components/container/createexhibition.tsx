import React, { Component } from "react";
import Carousel from "@brainhubeu/react-carousel";
import "@brainhubeu/react-carousel/lib/style.css";
import Footer2 from "../Footer/footer2";
import { Globaldata } from "./sample";
import { apiurlprefix } from "../../apiconfig";
import history from "../../history";
import api from "../API/api";
import Loader from './loader';

interface ICreateexhibition {
  exhibition: any;
  loading:boolean;
}

export class Createexhibition extends Component<{}, ICreateexhibition> {
  constructor(props: {}) {
    super(props);
    this.state = {
      exhibition: [],
      loading:true,
    };
  }
  clickid(e: any) {
    Globaldata.roomrentid = e.target.id;
    history.push(`/newexhibition/${Globaldata.roomrentid}`);
  }
  async onload() {
    const exblists = await api.exblist();
    this.setState({ exhibition: exblists,loading:false });
  }
  componentDidMount() {
    this.onload();
    // axios
    //   .post(`${apiurlprefix}/exblist`)
    //   .then((res) => {
    //   this.setState({exhibition:res.data})
    //   });
  }
  render() {
    return (
      <div>
        <div className="wrapper light-wrapper">
          <div className="container inner pt-60">
            <h1 className="heading text-center">Exhibition templates</h1>
            <div className="space40"></div>
            <div className="createExb_img">
              {this.state.loading?<Loader/>:
              <Carousel slidesPerPage={2} arrows infinite>
                {this.state.exhibition.map((item: any) => {
                  return item.amt == 0 ? (
                    <button
                      className="button_slider payment_link"
                      key={item.id}
                      id={item.id}
                      onClick={(e) => this.clickid(e)}
                    >
                      <img
                        src={`${apiurlprefix}/` + item.image}
                        className="caruosel_img"
                        alt=""
                        id={item.id}
                        onClick={(e) => this.clickid(e)}
                      />
                      Free
                    </button>
                  ) : (
                    <button
                      className="button_slider payment_link"
                      key={item.id}
                      id={item.id}
                      disabled={true}
                    >
                      <img
                        src={`${apiurlprefix}/` + item.image}
                        className="caruosel_img"
                        alt=""
                      />
                      Buy this gallery
                    </button>
                  );
                })}
              </Carousel>}
            </div>
          </div>
        </div>
        <Footer2 />
      </div>
    );
  }
}

export default Createexhibition;
