import React, { Component } from "react";
import Footer from "../Footer/footer";
import axios from "axios";
import Lightbox from "react-image-lightbox";
import "react-image-lightbox/style.css";
import VirtualUrl from "../../VirtualGalleryConfig.json";
import { apiurlprefix } from "../../apiconfig";
import api from "../API/api";
import {isMobile} from "react-device-detect";

interface ISearch {
  searchresults: any;
  likedimgid: any;
  photoIndex: number;
  isOpen: boolean;
  likesCount: any;
  imageViewCount: any;
  exhibitionViewCount: any;
  exhibitionlikesCount: any;
  loading: boolean;
  iframeurl: string;
  isLoggedIn:boolean;
  imagesUrl:any;
  windowWidth:any;
}

class Search extends Component<{}, ISearch> {
  constructor(props: {}) {
    super(props);
    this.state = {
      searchresults: [],
      likedimgid: [],
      photoIndex: 0,
      isOpen: false,
      likesCount: [],
      imageViewCount: [],
      exhibitionViewCount: [],
      exhibitionlikesCount: [],
      loading: true,
      iframeurl: "",
      isLoggedIn:false,
      imagesUrl:[],
      windowWidth: window.innerWidth
    };
  }
  handleResize = (e:Event) => {
    this.setState({ windowWidth: window.innerWidth });
   };
  iframeload(userid: number, exbid: number) {
    let logonUserId = 0;
    if(localStorage.getItem('userid') !== null){
      logonUserId = parseInt(localStorage.getItem('userid') || '');
    }
    this.setState({
      iframeurl:
        `${apiurlprefix}/` +
        VirtualUrl.virtualGalleryUrl +
        `?userid=${userid}&exhibitionid=${exbid}&loggeduserid=${logonUserId}`,
    });
  }

  imageCount(id: number) {
    let selectedIndex = 0;
    for (let i = 0; i < this.state.searchresults.length; i++) {
      if (id == this.state.searchresults[i].image_id) {
        selectedIndex = i;
      }
    }
    this.setState({ isOpen: true, photoIndex: selectedIndex }, () => {
      this.selectedImage(this.state.photoIndex);
    });
  }

  ExhibitionViewCount(index: number,id: number){
      let viewCountArray =this.state.exhibitionViewCount;
      let count = viewCountArray[index]
      count++;
      viewCountArray[index]=count;
      this.setState({ exhibitionViewCount: viewCountArray });
      this.Exbincressview(id);
  }

  async Exbincressview(exbId: number) {
    let res = await api.increaseexhibitionViewCount(exbId);
  }

  
  selectedImage(n: number) {
    let countvalues = this.state.imageViewCount;
    let count = countvalues[n];
    let c = 0;
    if (c == 0) {
    }
    count++;
    countvalues[n] = count;
    this.setState({ imageViewCount: countvalues });
    this.viewCount(this.state.searchresults[this.state.photoIndex].image_id);
  }
  async like(id: number, index: number) {
    this.setState({ likedimgid: 0 });
    if (localStorage.getItem("logonid") !== null) {
      let selectedcount = this.state.likesCount[index];
      let data = this.state.likesCount;
      if ($(`#${id}`).hasClass("mystyle")) {
        // console.log(id);
        selectedcount--;
        data[index] = selectedcount;
        this.setState({ likesCount: data });
        var element = document.getElementById(`${id}`);
        if (element != null) {
          element.classList.toggle("mystyle");
        }
      } else {
        selectedcount++;
        data[index] = selectedcount;
        this.setState({ likesCount: data });
        var element = document.getElementById(`${id}`);
        if (element != null) {
          element.classList.toggle("mystyle");
        }
      }
      
      let res = await api.UserImageLike(id);
    } else {
      $(".login_button").click();
      let winwid = $(window).width() || 0;
      if(winwid<=991){
        $(".navbar-toggler").click();
      }
    }
  }

  async Exblike(id: number, index: number){
    if (localStorage.getItem("logonid") !== null) {
      let selectedcount = this.state.exhibitionlikesCount[index];
      let data = this.state.exhibitionlikesCount;
      if ($(`#${id}`).hasClass("mystyle")) {
        // console.log(id);
        selectedcount--;
        data[index] = selectedcount;
        this.setState({ exhibitionlikesCount: data });
        var element = document.getElementById(`${id}`);
        if (element != null) {
          element.classList.toggle("mystyle");
        }
      } else {
        selectedcount++;
        data[index] = selectedcount;
        this.setState({ exhibitionlikesCount: data });
        var element = document.getElementById(`${id}`);
        if (element != null) {
          element.classList.toggle("mystyle");
        }
      }
      
      let res = await api.UserExbLike(id);
    } else {
      $(".login_button").click();
      let winwid = $(window).width() || 0;
      if(winwid<=991){
        $(".navbar-toggler").click();
      }
    }

  }

  likedimages() {
    for (let i = 0; i < this.state.likedimgid.length; i++) {
      // console.log(this.state.likedimgid[i].imageid);
      var element = document.getElementById(
        `${this.state.likedimgid[i].imageid}`
      );
      if (element != null) {
        if ($(element).hasClass("mystyle")) {
        } else {
          element.classList.toggle("mystyle");
        }
      }
    }
  }
  previous() {
    let str = $(".ril-image-prev").attr("src") || "";
    console.log("previous: " + str);
    var sliced = str.slice(apiurlprefix.length + 1);
    for (let i = 0; i < this.state.searchresults.length; i++) {
      let countvalues = this.state.imageViewCount;
      let count = countvalues[i];
      if (sliced === this.state.searchresults[i].image_image) {
        count++;
        countvalues[i] = count;
        this.setState({ imageViewCount: countvalues });
        this.viewCount(this.state.searchresults[i].image_id);
      }
    }
  }
  InfoMsg(){
    alert("For the best experience please use a laptop or fesktop computer. The interactive viewer is not supported on mobile devices yet, but stay tuned for the mobile app.");
  }
  
  next() {
    let str = $(".ril-image-next").attr("src") || "";
    console.log("next: " + str);
    var sliced = str.slice(apiurlprefix.length + 1);
    for (let i = 0; i < this.state.searchresults.length; i++) {
      let countvalues = this.state.imageViewCount;
      let count = countvalues[i];
      if (sliced == this.state.searchresults[i].image_image) {
        count++;
        countvalues[i] = count;
        this.setState({ imageViewCount: countvalues });
        this.viewCount(this.state.searchresults[i].image_id);
      }
    }
  }
  async viewCount(imageid: number) {
    let res = await api.increaseViewCount(imageid);
  }
  componentWillReceiveProps(newProps: any) {
    let urlprops = JSON.parse(JSON.stringify(newProps));
    this.onload(urlprops);
  }
  componentDidMount() {
    let urlprops = JSON.parse(JSON.stringify(this.props));    
    this.onload(urlprops);
    window.addEventListener("resize", this.handleResize);
  }
  componentWillUnmount() {
    window.addEventListener("resize", this.handleResize);
   } 
  async onload(urlprops: any) {
    if (urlprops.match.params.type == "0") {
      let res = await api.getSearchImages(urlprops.match.params.data);
      this.setState({ searchresults: res});
      let likeCountTemp: any = [];
      let viewCountTemp: any = [];
      let images: any = [];
      for (let i = 0; i < res.length; i++) {
        likeCountTemp.push(
          isNaN(parseInt(res[i].count)) ? 0 : parseInt(res[i].count)
        );
        viewCountTemp.push(res[i].image_view);
        images.push(`${apiurlprefix}/`+res[i].image_image)
      }
      this.setState({
        likesCount: likeCountTemp,
        imageViewCount: viewCountTemp,
        imagesUrl: images,
        loading:false
      });
    } else {
      let res = await api.getSearchExb(urlprops.match.params.data);
      this.setState({ searchresults: res});
      let likeCountTemp: any = [];
      let viewCountTemp: any = [];
      // let images: any = [];
      for (let i = 0; i < res.length; i++) {
        likeCountTemp.push(
          isNaN(parseInt(res[i].count)) ? 0 : parseInt(res[i].count)
        );
        viewCountTemp.push(res[i].virtualexhibition_view);
        // images.push(`${apiurlprefix}/`+res[i].image_image)
      }
      
      this.setState({
        exhibitionlikesCount: likeCountTemp,
        exhibitionViewCount: viewCountTemp,
       // imagesUrl: images,
        loading:false
      });

   
    }
    if (localStorage.getItem("logonid") !== null) {
      let sam = await api.getCurrentUserid();
      let res = await api.getUserLikedImagesId();
      this.setState({ likedimgid: res });
    }
  }

  searchRender() {
    // console.log(this.props)
    let sam = JSON.stringify(this.props);
    let sam1 = JSON.parse(sam);
    //  console.log(sam1.match.params.data)
    if (sam1.match.params.type == "0") {
      return (
        <>
          {this.state.searchresults.map((item: any, index: number) => (
            <div className="card" key={item.image_id}>
              <div className="artist">
                {item.firstname == null ? "Anonymous" : item.firstname}
              </div>
              <figure
                // data-toggle="modal"
                // data-target={"#modal" + item.image_id}
                className="overlay overlay2"
                onClick={() => this.imageCount(item.image_id)}
              >
                <span className="bg"></span>
                <img src={`${apiurlprefix}/` + item.image_image} alt="" />
              </figure>
              <div>
              <button
                className="count"
                id={`${item.image_id}`}
                onClick={() => {
                  this.like(item.image_id, index);
                }}
              >
                <i className="fa fa-heart"></i>
              </button>
              {this.state.likesCount[index] == 0
                ? ""
                : this.state.likesCount[index]}
              <i className="fa fa-eye count"></i>
              {this.state.imageViewCount[index]}
              </div>
            </div>
          ))}
        </>
      );
    } else {
      const { windowWidth } = this.state; 
      return (
        <>
          {this.state.searchresults.map((item: any, index: number) => (
            <div className="card" key={item.virtualexhibition_id}>
              <div className="artist">{item.firstname}</div>
              {(!isMobile)?
              <figure
                data-toggle="modal"
                data-target="#myModal"
                data-backdrop="static"
                data-keyboard="false"
                className="overlay overlay2"
                onClick={() =>{
                  this.iframeload(item.user_id, item.virtualexhibition_id);this.ExhibitionViewCount(index,item.virtualexhibition_id);
                }
                }
              >
                <span className="bg"></span>
                <img
                  src={`${apiurlprefix}/` + item.virtualexhibition_avatar}
                  alt=""
                />
              </figure>
              :
              <figure
               
                className="overlay overlay2"
                onClick={() =>{
                  this.InfoMsg();
                }
                }
              >
                <span className="bg"></span>
                <img
                  src={`${apiurlprefix}/` + item.virtualexhibition_avatar}
                  alt=""
                />
              </figure>
              }
              <div>
              <button
                className="count"
                id={`${item.virtualexhibition_id}`}
                onClick={() => {
                  this.Exblike(item.virtualexhibition_id, index);
                }}
              >
                <i className="fa fa-heart"></i>
              </button>
              {this.state.exhibitionlikesCount[index] == 0
                ? ""
                : this.state.exhibitionlikesCount[index]}
              <i className="fa fa-eye count"></i>
              {this.state.exhibitionViewCount[index]}
              <div className="artist">{item.virtualtheme}</div>
              </div>
            </div>
          ))}
        </>
      );
    }
  }
  RenderSearchData() {
    if (this.state.searchresults.length == 0 && this.state.loading == false) {
      return (
        <div className="d-flex justify-content-center">
          <img src="/assets/images/notfound.png" />
        </div>
      );
    }
    if (this.state.searchresults.length > 0) {
      return (
        <div className="card-columns">
          {this.searchRender()}
          {this.likedimages()}
          <div className="modal" id="myModal" tabIndex={-1}>
            <div className="modal-dialog">
              <div className="modal-content">
                <iframe
                  src={this.state.iframeurl}
                  id="popup1"
                  title="popup"
                  className="modal-body"
                  style={{ width: "100%", minHeight: "550px" }}
                ></iframe>
                <div className="modal-footer">
                  <button
                    type="button"
                    className="btn btn-full-rounded"
                    data-dismiss="modal"
                  >
                    Close
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      );
    }
    return (
      <div className="d-flex justify-content-center">
        <img src="/assets/images/loader.gif" />
      </div>
    );
  }

  render() {
    
    let datasearch = JSON.parse(JSON.stringify(this.props));
    let searchdata = datasearch.match.params.data;

    let isOpen = this.state.isOpen;
    let photoIndex = this.state.photoIndex;
    
    // const images: string | any[] = [];

    // for (let i = 0; i < this.state.searchresults.length; i++) {
    //   let imageurl =
    //     `${apiurlprefix}/` + this.state.searchresults[i].image_image;
    //   images.push(imageurl);
    //   // img.push(imageurl)
    // }
   
    return (
      <div>
        <div className="wrapper gray-wrapper">
          <div className="container inner pt-60">
            <h2 className="section-title text-center">
              Search results for {searchdata}
            </h2>
            <div className="clearfix"></div>
            <div className="space20"></div>
            {this.RenderSearchData()}
            <div>
              {isOpen && (
                <Lightbox
                  mainSrc={this.state.imagesUrl[photoIndex]}
                  nextSrc={this.state.imagesUrl[(photoIndex + 1) % this.state.imagesUrl.length]}
                  prevSrc={
                    this.state.imagesUrl[(photoIndex + this.state.imagesUrl.length - 1) % this.state.imagesUrl.length]
                  }
                  onCloseRequest={() => this.setState({ isOpen: false })}
                  onMovePrevRequest={() => {
                    this.setState({
                      photoIndex:
                        (photoIndex + this.state.imagesUrl.length - 1) % this.state.imagesUrl.length,
                    });
                    this.previous();
                  }}
                  onMoveNextRequest={() => {
                    this.setState({
                      photoIndex: (photoIndex + 1) % this.state.imagesUrl.length,
                    });
                    this.next();
                  }}
                />
              )}
            </div>
          </div>
        </div>
        <Footer />
      </div>
    );
  }
}

export default Search;
