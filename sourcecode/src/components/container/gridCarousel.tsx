import React, { Component } from "react";
import Carousel from "react-grid-carousel";
import axios from "axios";
import {apiurlprefix} from '../../apiconfig';


interface IGridCarousel {
  count: number;
  images:any;
}

class GridCarousel extends Component<{}, IGridCarousel> {
  constructor(props: {}) {
    super(props);
    this.state = { count: 0,images:[{
      "id": 15,
      "image": "uploads/53/img-1589790299568.jpeg"
    },
    {
      "id": 16,
      "image": "uploads/53/img-1589790418256.jpeg"
    },
    {
      "id": 17,
      "image": "uploads/53/img-1589790481237.jpeg"
    },
    {
      "id": 18,
      "image": "uploads/53/img-1589790498196.jpeg"
    },
    {
      "id": 19,
      "image": "uploads/53/img-1589790515363.jpeg"
    },
    {
      "id": 20,
      "image": "uploads/53/img-1589790547052.jpeg"
    },
    {
      "id": 21,
      "image": "uploads/53/img-1589790594532.jpeg"
    }] };
    this.handlecheckChange = this.handlecheckChange.bind(this);
  }
  handlecheckChange(e: any) {
    if (e.target.checked) {
      this.setState({ count: this.state.count + 1 });
    } else {
      this.setState({ count: this.state.count - 1 });
    }
  }
  // componentWillMount() {
  //   axios
  //     .post(`${apiurlprefix}/getimages`, {
  //       userid:53
  //     })
  //     .then((res) => {console.log(res.data[0]);
  //       this.setState({images:res.data[0]});
  //     });
  // }

  render() {
    return (
        <>
        
      <Carousel cols={3} rows={2} gap={1} loop>
        {this.state.images.map((item: any) =>
        <Carousel.Item key={item.id}>
          <div className="select_img">
            <input
              type="checkbox"
              id={item.id}
              onChange={this.handlecheckChange}
              className='select_checkbox'
            />
            <label htmlFor={item.id} className="labelname">
              <img src={`${apiurlprefix}/`+item.image} />
            </label>
          </div>
        </Carousel.Item>
        )}
      </Carousel>
      <div className="col-lg-12 col-md-12 selected">
                <label className="mulifont">Selected : {this.state.count}/6</label>
                </div>
      </>
    );
  }
}

export default GridCarousel;
