import React, { Component } from "react";
import Footer2 from "../Footer/footer2";
import ReactTags from "react-tag-autocomplete";
import Popup from "reactjs-popup";
import api from "../API/api";

interface IUploadart {
  file: any;
  imagePreviewUrl: any;
  imgCat: any;
  tags: any;
  suggestions: any;
  title: string;
  desc: string;
  price: number;
  category: string;
  currentUserId: number;
  selectedOption: string;
  imgvalid: string;
  titlevalid: string;
  descvalid: string;
  catvalid: string;
  tagvalid: string;
  btnLoader: boolean;
}

class Uploadart extends Component<{}, IUploadart> {
  constructor(props: {}) {
    super(props);
    this.state = {
      file: "",
      imagePreviewUrl: "",
      imgCat: [],
      tags: [],
      suggestions: [],
      title: "",
      desc: "",
      price: 0,
      category: "",
      currentUserId: 0,
      selectedOption: "public",
      imgvalid: "",
      titlevalid: "",
      descvalid: "",
      catvalid: "",
      tagvalid: "tags",
      btnLoader: false,
    };
  }

  _handleImageChange(e: any) {
    e.preventDefault();
    let reader = new FileReader();
    let file = e.target.files[0];
    if (file != undefined) {
      if (!file.name.match(/\.(jpg|jpeg|png|gif)$/)) {
        this.setState({imgvalid:'imgvalid',imagePreviewUrl:''})
      } else {
        let fsize = Math.round((file.size / 1024));
        reader.onloadend = () => {
          if(fsize>20){
          this.setState({
            file: file,
            imagePreviewUrl: reader.result,
            imgvalid:''
          });
        }
        };
        if (file != undefined) {
          reader.readAsDataURL(file);
        }
      }
    }
  }
  handleDelete(i: any) {
    const tags = this.state.tags.slice(0);
    tags.splice(i, 1);
    this.setState({ tags }, () => {
      this.tagValidation();
    });
  }

  handleAddition(tag: any) {
    let already = this.state.tags;
    let match = 0;
    for (let i = 0; i < already.length; i++) {
      if (already[i].name == tag.name) {
        match++;
      }
    }
    if (match == 0) {
      const tags = [].concat(this.state.tags, tag);
      this.setState({ tags }, () => {
        this.tagValidation();
      });
    }

  }

  tagValidation() {
    if (this.state.tags.length == 0) {
      this.setState({ tagvalid: "tags tagvalid" });
    } else {
      this.setState({ tagvalid: "tags" });
    }
  }

  title(e: any) {
    this.setState({ title: e.target.value });
  }
  desc(e: any) {
    this.setState({ desc: e.target.value });
    if (e.target.value == "") {
      this.setState({ descvalid: "1px solid red" });
    } else {
      this.setState({ descvalid: "1px solid rgba(21, 21, 21, 0.15)" });
    }
  }
  price(e: any) {
    this.setState({ price: e.target.value });
  }
  category(e: any) {
    this.setState({ category: e.target.value });
    console.log(e.target.value);
    if (e.target.value == "--Select the category--") {
      this.setState({ catvalid: "1px solid red" });
    } else {
      this.setState({ catvalid: "1px solid rgba(21, 21, 21, 0.15)" });
    }
  }
  handleOptionChange(e: any) {
    this.setState({ selectedOption: e.target.value });
  }

  handleSubmit(e: any) {
    e.preventDefault();
    this.setState({btnLoader:true})
    let categoryId;
    for (let i = 0; i < this.state.imgCat.length; i++) {
      if (this.state.imgCat[i].name == this.state.category) {
        categoryId = this.state.imgCat[i].id;
      }
    }
    let tempTag = this.state.tags;
    let tagid = "";
    for (let i = 0; i < tempTag.length; i++) {
      if (tempTag[i].id != undefined) {
        tagid = tagid + tempTag[i].id;

        if (tempTag.length - 1 != i) {
          tagid = tagid + ",";
        }
      }
    }

    let str = tagid;
    let tagsrc = str.length;
    let tagids = "";
    if (str[tagsrc - 1] == ",") {
      tagids = str.substring(0, str.length - 1);
    }
    let inserttags = "";

    for (let i = 0; i < tempTag.length; i++) {
      let j;
      for (j = 0; j < this.state.suggestions.length; j++)
        if (tempTag[i].name == this.state.suggestions[j].name) break;

      if (j == this.state.suggestions.length) {
        inserttags = inserttags + "," + tempTag[i].name;
      }
    }
    let finaltags;
    if (tagids == "") {
      finaltags = inserttags == "" ? tagid : tagid + "," + inserttags;
    } else {
      finaltags = inserttags == "" ? tagids : tagids + "," + inserttags;
    }
    // tagids = (inserttags=='')?tagids:tagids+','+inserttags;
    let formValidate = true;
    if (this.state.imagePreviewUrl == "") {
      this.setState({ imgvalid: "imgvalid" });
      formValidate = false;
    } else {
      this.setState({ imgvalid: "" });
    }

    if (this.state.desc == "") {
      this.setState({ descvalid: "1px solid red" });
      formValidate = false;
    } else {
      this.setState({ descvalid: "1px solid rgba(21, 21, 21, 0.15)" });
    }

    if (categoryId == undefined) {
      this.setState({ catvalid: "1px solid red" });
      formValidate = false;
    } else {
      this.setState({ catvalid: "1px solid rgba(21, 21, 21, 0.15)" });
    }
    if (finaltags == "") {
      this.setState({ tagvalid: "tags tagvalid" });
      formValidate = false;
    } else {
      this.setState({ tagvalid: "tags" });
    }

    if (formValidate) {
      const datas = {
        image: this.state.imagePreviewUrl,
        title: this.state.title,
        desc: this.state.desc,
        price: this.state.price,
        imguserid: this.state.currentUserId,
        category: categoryId,
        imgClassifiedAs: this.state.selectedOption,
        tagid: finaltags,
      };

      this.setState({
        title: "",
        desc: "",
        price: 0,
        imagePreviewUrl: "",
        tags: [],
        category: "--Select the category--",
      });
      this.uploadArt(datas);
    }
    else{
        this.setState({btnLoader:false})
    }
    e.preventDefault();
  }
  uplaodSuccess() {
    $(".modal_close").click();
  }
  async uploadArt(datas: any) {
    let success = api.uploadArt(datas);
      this.setState({btnLoader:false})
      $(".uploadart_btn").click();
    return;
  }
  async onload() {
    const suggestions = await api.getimgTags();
    const imgCategory = await api.getimgCategory();
    const userid = await api.getCurrentUserid();
    this.setState({
      suggestions: suggestions,
      imgCat: imgCategory,
      currentUserId: userid,
    });
  }
  componentDidMount() {
    this.onload();
  }
  render() {
    let { imagePreviewUrl } = this.state;
    let imagePreview = null;

    if (imagePreviewUrl) {
      imagePreview = <img src={imagePreviewUrl} />;
    } else {
      imagePreview = (
        <div className={`previewText text-center ${this.state.imgvalid}`}>
          Please select an Image for Preview
        </div>
      );
    }
    return (
      <div>
        <div className="wrapper gray-wrapper">
          <div className="container inner pt-60">
            <h1 className="heading text-center">
              Upload your art to get acknowledged by the world
            </h1>
            <div className="space40"></div>
            <div className="row mulifont">
              <div className="col-lg-6 col-xl-6 col-md-7">
                <form onSubmit={(e) => this.handleSubmit(e)}>
                  <div className="form-group row">
                    <label
                      htmlFor="file"
                      className="col-sm-3 col-md-4 col-form-label"
                    >
                      Upload Art
                    </label>
                    <div className="col-sm-7">
                      <input
                        name="file"
                        className="fileInput"
                        type="file"
                        onChange={(e) => this._handleImageChange(e)}
                      />
                    </div>
                  </div>
                  <div className="form-group row mobimg_preview">
                    <div className="col-md-12">
                      <div className="imgPreview">{imagePreview}</div>
                    </div>
                  </div>
                  <div className="form-group row">
                    <label
                      htmlFor="title"
                      className="col-sm-3 col-md-4 col-form-label"
                    >
                      Image Title
                    </label>
                    <div className="col-sm-7">
                      <input
                        type="text"
                        className="form-control"
                        name="title"
                        value={this.state.title}
                        onChange={(e) => this.title(e)}
                        placeholder="Title"
                        style={{ border: this.state.titlevalid }}
                      />
                    </div>
                  </div>
                  <div className="form-group row">
                    <label
                      htmlFor="desc"
                      className="col-sm-3 col-md-4 col-form-label"
                    >
                      Description
                    </label>
                    <div className="col-sm-7">
                      <textarea
                        name="desc"
                        id="desc"
                        cols={30}
                        rows={10}
                        value={this.state.desc}
                        onChange={(e) => this.desc(e)}
                        style={{ border: this.state.descvalid }}
                      ></textarea>
                    </div>
                  </div>
                  <div className="form-group row">
                    <label
                      htmlFor="price"
                      className="col-sm-3 col-md-4 col-form-label"
                    >
                      Price (USD)
                    </label>
                    <div className="col-sm-7">
                      <input
                        type="number"
                        name="price"
                        placeholder="0"
                        value={this.state.price}
                        onChange={(e) => this.price(e)}
                        className="form-control"
                      />
                    </div>
                  </div>
                  <div className="form-group row">
                    <label htmlFor="title" className="col-sm-4 col-form-label">
                      Private/Public
                    </label>
                    <div className="col-sm-7 pub-private">
                      <div className="row plan">
                        <div className="custom-control custom-radio custom-control-inline col-lg-3 col-md-4 col-sm-3 col-4">
                          <input
                            type="radio"
                            id="private"
                            name="publicPrivate"
                            className="custom-control-input"
                            value="private"
                            checked={this.state.selectedOption === "private"}
                            onChange={(e) => this.handleOptionChange(e)}
                          />
                          <label
                            className="custom-control-label"
                            htmlFor="private"
                          >
                            Private
                          </label>
                        </div>
                        <div className="custom-control custom-radio custom-control-inline col-lg-3 col-md-4 col-sm-3 col-4">
                          <input
                            type="radio"
                            id="public"
                            name="publicPrivate"
                            className="custom-control-input"
                            value="public"
                            checked={this.state.selectedOption === "public"}
                            onChange={(e) => this.handleOptionChange(e)}
                          />
                          <label
                            className="custom-control-label"
                            htmlFor="public"
                          >
                            Public
                          </label>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="form-group row cat_form">
                    <label
                      htmlFor="title"
                      className="col-sm-3 col-md-4 col-form-label"
                    >
                      Category
                    </label>
                    <div className="col-sm-7 custom-select-wrapper">
                      <select
                        className="custom-select"
                        value={this.state.category}
                        onChange={(e) => this.category(e)}
                        style={{ border: this.state.catvalid }}
                      >
                        <option defaultValue="">--Select the category--</option>
                        {this.state.imgCat.map((obj: any) => (
                          <option key={obj.id} value={obj.name}>
                            {obj.name}
                          </option>
                        ))}
                      </select>
                    </div>
                  </div>
                  <div className="form-group row tags_column">
                    <label
                      htmlFor="title"
                      className="col-sm-3 col-md-4 col-form-label"
                    >
                      Tag
                    </label>
                    <div className="col-sm-7">
                      <div className={this.state.tagvalid}>
                        <ReactTags
                          tags={this.state.tags}
                          suggestions={this.state.suggestions}
                          handleDelete={this.handleDelete.bind(this)}
                          handleAddition={this.handleAddition.bind(this)}
                          autofocus={false}
                          allowNew={true}
                        />
                      </div>
                    </div>
                  </div>
                  <div className="form-group row">
                    <div className="col-sm-12">
                      <button
                        type="submit"
                        className="btn btn-full-rounded"
                        disabled={this.state.btnLoader}
                      >
                        {this.state.btnLoader && (
                          <i
                            className="fa fa-refresh fa-spin"
                            style={{ marginRight: "5px" }}
                          />
                        )}
                        Upload
                      </button>
                    </div>
                  </div>
                </form>
              </div>
              <div className="col-lg-6 col-xl-6 col-md-5 img_preview">
                <div className="imgPreview">{imagePreview}</div>
              </div>
            </div>
          </div>
          <Popup trigger={<button className="uploadart_btn"></button>} modal>
            {(close) => (
              <div>
                <button className="close modal_close" onClick={close}>
                  &times;
                </button>
                <div className="col-lg-8 col-sm-8 col-12 main-section">
                  <div className="modal-content">
                    <div className="col-lg-12 col-sm-12 col-12 user-name text-center">
                      <h5>Art Uploaded Successfully.</h5>
                    </div>
                    <div className="col-lg-12 col-sm-12 col-12 form-input text-center">
                      <button
                        className="btn btn-full-rounded"
                        onClick={this.uplaodSuccess}
                      >
                        OK
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            )}
          </Popup>
        </div>
        <Footer2 />
      </div>
    );
  }
}

export default Uploadart;
