import api from '../API/api';
export interface PhotoProps {
    alt: string;
    category?: string;
    imageUrl: string;
    caption?: string;
}

export const imgPrefix = '/assets/images/';

export const ExhibitionData: PhotoProps[] = [
    {
        category: 'NATURE',
        imageUrl: `${imgPrefix}art/ib1.jpg`,
        alt: 'Paris',
        caption: 'Paris',
    },
    {
        category: 'LANDSCAPE',
        imageUrl: `${imgPrefix}art/ib2.jpg`,
        alt: 'Stockholm',
        caption: 'Stockholm',
    },
    {
        category: 'ART',
        imageUrl: `${imgPrefix}art/ib3.jpg`,
        alt: 'Venice',
        caption: 'Venice',
    },
    {
        category: 'FUN',
        imageUrl: `${imgPrefix}art/ib4.jpg`,
        alt: 'Amsterdam',
        caption: 'Amsterdam',
    },
    {
        category: 'URBAN',
        imageUrl: `${imgPrefix}art/ib5.jpg`,
        alt: 'Copenhagen',
        caption: 'Copenhagen',
    },
    {
        category: 'ART',
        imageUrl: `${imgPrefix}art/ib6.jpg`,
        alt: 'Rio de janeiro',
        caption: 'Rio de janeiro',
    },
    {
        category: 'LANDSCAPE',
        imageUrl: `${imgPrefix}art/ib7.jpg`,
        alt: 'San Francisco',
        caption: 'San Francisco',
    },
    {
        category: 'NATURE',
        imageUrl: `${imgPrefix}art/ib8.jpg`,
        alt: 'Valencia',
        caption: 'Valencia',
    },
    {
        category: 'FUN',
        imageUrl: `${imgPrefix}art/ib9.jpg`,
        alt: 'Tokyo',
        caption: 'Tokyo',
    },
    {
        category: 'ART',
        imageUrl: `${imgPrefix}art/ib10.jpg`,
        alt: 'Bangkok',
        caption: 'Bangkok',
    },
    {
        category: 'LANDSCAPE',
        imageUrl: `${imgPrefix}art/ib11.jpg`,
        alt: 'Giza',
        caption: 'Giza',
    },
    {
        category: 'NATURE',
        imageUrl: `${imgPrefix}art/ib12.jpg`,
        alt: 'Montreal',
        caption: 'Montreal',
    },
];
