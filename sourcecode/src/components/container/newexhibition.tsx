import React, { Component } from "react";
import Footer2 from "../Footer/footer2";
import "@brainhubeu/react-carousel/lib/style.css";
import VirtualUrl from "../../VirtualGalleryConfig.json";
import { Link } from "react-router-dom";
import axios from "axios";
import ReactTags from "react-tag-autocomplete";
import Carousel from "react-grid-carousel";
import { Globaldata } from "./sample";
import { apiurlprefix } from "../../apiconfig";
import api from '../API/api';
import history from '../../history';
import Loader from './loader';



interface INewexhibition {
  count: number;
  imgCat: any;
  tags: any;
  suggestions: any;
  imageslist: any;
  category: string;
  imageid: number[];
  title: string;
  name: string;
  desc: string;
  selectedOption: string;
  currentUserId: number;
  exbID: number;
  selectedroom:any;
  imgvalid:string;
  titlevalid:string;
  catvalid: string;
  tagvalid: string;
  iframeurl:string;
  limit:number;
  btnLoader:boolean;
  loading:boolean;
}

class Newexhibition extends Component<{}, INewexhibition> {
  constructor(props: {}) {
    super(props);
    this.state = {
      count: 0,
      imgCat: [],
      tags: [],
      suggestions: [],
      imageslist: [],
      category: "",
      imageid: [],
      desc: "",
      title: "",
      name: "",
      selectedOption: "public",
      currentUserId: 0,
      exbID: 0,
      selectedroom:[],
      imgvalid:'',
      titlevalid:'',
      catvalid: "",
      tagvalid: "tags",
      iframeurl:'',
      limit:0,
      btnLoader:false,
      loading:true
    };
    this.handlecheckChange = this.handlecheckChange.bind(this);
  }
  handlecheckChange(e: any) {
    if (e.target.checked) {
      if (this.state.count > this.state.limit-1) {
        e.target.checked = false;
      } else {
        this.setState({ count: this.state.count + 1 },()=>{this.imageValidation()});
        const array = this.state.imageid;
        array.push(e.target.id);
        this.setState({
          imageid: array,
        });
      }
    } else {
      this.setState({ count: this.state.count - 1 },()=>{this.imageValidation()});
      const array = this.state.imageid;
      const index = array.indexOf(e.target.id);
      array.splice(index, 1);
      this.setState({
        imageid: array,
      });
    }
    
  }
  handleDelete(i: any) {
    const tags = this.state.tags.slice(0);
    tags.splice(i, 1);
    this.setState({ tags },()=>{this.tagValidation()});
  }

  handleAddition(tag: any) {
    let already = this.state.tags;
    let match = 0;
    for (let i = 0; i < already.length; i++) {
      if (already[i].name == tag.name) {
        match++;
      }
    }
    if (match == 0) {
      const tags = [].concat(this.state.tags, tag);
      this.setState({ tags },()=>{this.tagValidation()});
    }
  }
  tagValidation(){
    if(this.state.tags.length == 0){
      this.setState({tagvalid:'tags tagvalid'})
    }
    else{
      this.setState({tagvalid:'tags'})
    }
  }
  imageValidation(){
    if(this.state.count>0){
      this.setState({imgvalid:''})
    }
    else{
      this.setState({imgvalid:'imgvalid'})
    }
  }
  title(e: any) {
    this.setState({ title: e.target.value });
    if (e.target.value == "") {
      this.setState({ titlevalid: "1px solid red" });
    } else {
      this.setState({ titlevalid: "1px solid rgba(21, 21, 21, 0.15)" });
    }
  }
  desc(e: any) {
    this.setState({ desc: e.target.value });
  }
  name(e: any) {
    this.setState({ name: e.target.value });
  }
  category(e: any) {
    this.setState({ category: e.target.value });
    if (e.target.value == "--Select the category--") {
      this.setState({ catvalid: "1px solid red" });
    } else {
      this.setState({ catvalid: "1px solid rgba(21, 21, 21, 0.15)" });
    }
  }
  handleOptionChange(e: any) {
    this.setState({ selectedOption: e.target.value });
  }
  handleSubmit(e: any) {
    this.setState({btnLoader:true})
    let imgidstr = "";
    let ids = this.state.imageid;
    for (let i = 0; i < ids.length; i++) {
      imgidstr = imgidstr + ids[i];
      if (ids.length - 1 == i) {
      } else {
        imgidstr = imgidstr + ";";
      }
    }
    let firstavatar = ids[0];
    let firstpath;
    for (let i = 0; i < this.state.imageslist.length; i++) {
      let id = this.state.imageslist[i].id;
      if (id == firstavatar) {
        firstpath = this.state.imageslist[i].image;
      }
    }

    let categoryId;
    for (let i = 0; i < this.state.imgCat.length; i++) {
      if (this.state.imgCat[i].name == this.state.category) {
        categoryId = this.state.imgCat[i].id;
      }
    }
   
    let prvpub = this.state.selectedOption == "public" ? 1 : 0;

    let tempTag = this.state.tags;
    let tagid = "";
    for (let i = 0; i < tempTag.length; i++) {
      if (tempTag[i].id != undefined) {
        tagid = tagid + tempTag[i].id;

        if (tempTag.length - 1 != i) {
          tagid = tagid + ",";
        }
      }
    }
    let str = tagid;
    let tagsrc = str.length;
    let tagids = "";
    if (str[tagsrc - 1] == ",") {
      tagids = str.substring(0, str.length - 1);
    }

    let inserttags = "";

    for (let i = 0; i < tempTag.length; i++) {
      let j;
      for (j = 0; j < this.state.suggestions.length; j++)
        if (tempTag[i].name == this.state.suggestions[j].name) break;

      if (j == this.state.suggestions.length) {
        inserttags = inserttags + "," + tempTag[i].name;
      }
    }
    let finaltags;
    if (tagids == "") {
      finaltags = inserttags == "" ? tagid : tagid + "," + inserttags;
    } else {
      finaltags = inserttags == "" ? tagids : tagids + "," + inserttags;
    }

    let urlprops = JSON.parse(JSON.stringify(this.props));
    let roomrentid = urlprops.match.params.rentid;

    let formValidate = true;
    if(imgidstr == ''){
      this.setState({imgvalid:'imgvalid'})
      formValidate=false;
    }
    else{
      this.setState({imgvalid:''})
    }
    if (this.state.title == "") {
      this.setState({ titlevalid: "1px solid red" });
      formValidate = false;
    } else {
      this.setState({ titlevalid: "1px solid rgba(21, 21, 21, 0.15)" });
    }
    if (categoryId == undefined) {
      this.setState({ catvalid: "1px solid red" });
      formValidate = false;
    } else {
      this.setState({ catvalid: "1px solid rgba(21, 21, 21, 0.15)" });
    }
    if (finaltags == "") {
      this.setState({tagvalid:'tags tagvalid'})
      formValidate = false;
    } else {
      this.setState({tagvalid:'tags'})
    }
       
    if (formValidate) {
      const datas = {
        imgids: imgidstr,
            rentid: roomrentid,
            virtualExbAvatar: firstpath,
            virtualExbTheme: this.state.title,
            desc: this.state.desc,
            name: this.state.name,
            userid: this.state.currentUserId,
            virtualExbprvpub: prvpub,
            categoryid: categoryId,
            tagid: finaltags,
      }
      this.submitsend(datas);
      var str2 = imgidstr;
       var resid = str2.split(";");
       
    for(let i=0; i<=resid.length; i++) {
      $(`#${resid[i]}`).prop("checked", false);
    }  
      this.setState({count:0,title:'',desc:'',name:'',category:'--Select the category--',tags:[]});
    }
    else{
      this.setState({btnLoader:false})
    }
    
    e.preventDefault();
  }
  async submitsend(datas:any){
    let exbid = await api.createexb(datas);
    this.setState({btnLoader:false})
    this.setState({exbID:exbid})
    let logonUserId = 0;
    if(localStorage.getItem('userid') !== null){
      logonUserId = parseInt(localStorage.getItem('userid') || '');
    }
    this.setState({iframeurl:`${apiurlprefix}/`+
    VirtualUrl.virtualGalleryUrl +
    `?userid=${this.state.currentUserId}&exhibitionid=${this.state.exbID}&loggeduserid=${logonUserId}`});
    $(".create_exb_btn").click();
    return;
  }
  async onload(){
    const suggestions = await api.getexbTags();
    const imgCategory = await api.getexbCategory();
    let userid = parseInt(localStorage.getItem("userid") || "{}");
    this.setState({ currentUserId: userid });
    const exbimages = await api.getexbImages();
    let urlprops = JSON.parse(JSON.stringify(this.props));
    let roomrentid = urlprops.match.params.rentid;
    const selectedroom = await api.getSelectedRoom(roomrentid);
    this.setState({suggestions:suggestions,imgCat:imgCategory,imageslist:exbimages,selectedroom:selectedroom.exb_image,limit:selectedroom.virtual_room_rent_no_of_images,loading:false});
  }
  componentDidMount() {
    this.onload();
  }
  
  handleKeyPress(){
    $("#myModal").on("hidden.bs.modal", function () {
      history.push('/createexhibition');
  });
  }

  render() {
    let settings = [
      {
        breakpoint: 800,
        cols: 3,
        rows: 1,
        gap: 10,
        loop: true,
      }
    ];
    return (
      <div className="wrapper gray-wrapper">
        <div className="container inner pt-60 container_newexb">
          <h1 className="heading text-center">New Exhibition</h1>
          <div className="space40"></div>
          <div className="row">
            <div className="col-lg-7 col-md-6">
              {this.state.loading?<Loader/>:
              <Carousel cols={3} rows={2} gap={1} loop settings={settings}>
                {this.state.imageslist.map((item: any) => (
                  <Carousel.Item key={item.id}>
                    <div className="select_img">
                      <input
                        type="checkbox"
                        id={item.id}
                        onClick={this.handlecheckChange}
                        className="select_checkbox"
                      />
                      <label htmlFor={item.id} className="labelname">
                        <img src={`${apiurlprefix}/` + item.image} />
                      </label>
                    </div>
                  </Carousel.Item>
                ))}
              </Carousel>}
              <div className="col-lg-12 col-md-12 selected">
                <label className={`mulifont ${this.state.imgvalid}`}>
                  Selected : {this.state.count}/{this.state.limit}
                </label>
              </div>
            </div>
            <div className="col-lg-5 col-md-6 carousel_newexb">
              {this.state.loading?<Loader/>:
              <img src={`${apiurlprefix}/${this.state.selectedroom}`} />}
            </div>
          </div>
          <div className="row mulifont">
            <div className="col-lg-10 col-xl-8">
              <form onSubmit={(e) => this.handleSubmit(e)}>
                <div className="form-group row">
                  <label htmlFor="title" className="col-sm-3 col-form-label">
                    Exhibition Title
                  </label>
                  <div className="col-sm-7">
                    <input
                      type="text"
                      className="form-control"
                      name="title"
                      placeholder="Title"
                      value={this.state.title}
                      onChange={(e) => this.title(e)}
                      style={{border:this.state.titlevalid}}
                    />
                  </div>
                </div>
                <div className="form-group row">
                  <label htmlFor="desc" className="col-sm-3 col-form-label">
                    Description
                  </label>
                  <div className="col-sm-7">
                    <textarea
                      name="desc"
                      id=""
                      cols={30}
                      rows={10}
                      value={this.state.desc}
                      onChange={(e) => this.desc(e)}
                    ></textarea>
                  </div>
                </div>
                <div className="form-group row">
                  <label htmlFor="name" className="col-sm-3 col-form-label">
                    Display Name
                  </label>
                  <div className="col-sm-7">
                    <input
                      type="text"
                      name="name"
                      placeholder="Name"
                      className="form-control"
                      value={this.state.name}
                      onChange={(e) => this.name(e)}
                    />
                  </div>
                </div>
                <div className="form-group row">
                  <label htmlFor="title" className="col-sm-3 col-form-label">
                    Private/Public
                  </label>
                  <div className="col-sm-7 pub-private">
                    <div className="row plan">
                      <div className="custom-control custom-radio custom-control-inline col-lg-3 col-md-4 col-sm-3 col-4">
                        <input
                          type="radio"
                          id="private"
                          name="publicPrivate"
                          className="custom-control-input"
                          value="private"
                          checked={this.state.selectedOption === "private"}
                          onChange={(e) => this.handleOptionChange(e)}
                        />
                        <label
                          className="custom-control-label"
                          htmlFor="private"
                        >
                          Private
                        </label>
                      </div>
                      <div className="custom-control custom-radio custom-control-inline col-lg-3 col-md-4 col-sm-3 col-4">
                        <input
                          type="radio"
                          id="public"
                          name="publicPrivate"
                          className="custom-control-input"
                          value="public"
                          checked={this.state.selectedOption === "public"}
                          onChange={(e) => this.handleOptionChange(e)}
                        />
                        <label
                          className="custom-control-label"
                          htmlFor="public"
                        >
                          Public
                        </label>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="form-group row cat_form">
                  <label htmlFor="title" className="col-sm-3 col-form-label">
                    Category
                  </label>
                  <div className="col-sm-7 custom-select-wrapper">
                    <select
                      className="custom-select"
                      value={this.state.category}
                      onChange={(e) => this.category(e)}
                      style={{border:this.state.catvalid}}
                    >
                      <option defaultValue="">--Select the category--</option>
                      {this.state.imgCat.map((obj: any) => (
                        <option key={obj.id} value={obj.name}>
                          {obj.name}
                        </option>
                      ))}
                    </select>
                  </div>
                </div>
                <div className="form-group row tags_column">
                  <label htmlFor="title" className="col-sm-3 col-form-label">
                    Tag
                  </label>
                  <div className="col-sm-7">
                    <div className={this.state.tagvalid}>
                      <ReactTags
                        tags={this.state.tags}
                        suggestions={this.state.suggestions}
                        handleDelete={this.handleDelete.bind(this)}
                        handleAddition={this.handleAddition.bind(this)}
                        autofocus={false}
                        allowNew={true}
                      />
                    </div>
                  </div>
                </div>
                <div className="form-group row">
                  <div className="col-sm-12 col-12 newexb_btns">
                  <button type="submit" className="btn btn-full-rounded" disabled={this.state.btnLoader}>
                      {this.state.btnLoader && (<i
              className="fa fa-refresh fa-spin"
              style={{ marginRight: "5px" }}
            />)}Create Exhibition
                      </button>
                    <Link to="/uploadart">
                      <button className="btn btn-full-rounded up_art">
                        Browse & Upload Art
                      </button>
                    </Link>
                  </div>
                </div>
                <button
                  className="create_exb_btn"
                  data-toggle="modal"
                  data-target="#myModal"
                  data-backdrop="static" data-keyboard="false"
                ></button>
                <div className="modal hello" id="myModal" tabIndex={-1}>
                  {this.handleKeyPress()}
                  <div className="modal-dialog">
                    <div className="modal-content">
                      <iframe
                        src={this.state.iframeurl}
                        id="popup1"
                        title="popup"
                        className="modal-body"
                        style={{ width: "100%", minHeight: "550px" }}
                      ></iframe>
                      <div className="modal-footer">
                        <button
                          type="button"
                          className="btn btn-full-rounded"
                          data-dismiss="modal"
                        >
                          Close
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
        <Footer2 />
      </div>
    );
  }
}


export default Newexhibition;
