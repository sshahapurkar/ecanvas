import React,{Component} from 'react';
import VirtualUrl from '../../VirtualGalleryConfig.json';
import {apiurlprefix} from '../../apiconfig';
import api from '../API/api';
import Loader from '../container/loader';
import {isMobile} from "react-device-detect";

interface IAdminslider{
  loading:boolean;
  exblist:any;
  iframeurl:string;
  windowWidth:any;
} 

export class Adminslider extends Component<{},IAdminslider>{
    constructor(props: {}){
      super(props)
      this.state = {
        loading:true,
        exblist:[],
        iframeurl:'',
        windowWidth: window.innerWidth
      }
    }
    handleResize = (e:Event) => {
      this.setState({ windowWidth: window.innerWidth });
     };
    async onload(){
      let res = await api.getAllExb();
      this.setState({exblist:res,loading:false});
    }
    componentDidMount(){
      this.onload();
      window.addEventListener("resize", this.handleResize);
    }
    componentWillUnmount() {
      window.addEventListener("resize", this.handleResize);
     } 
     InfoMsg(){
      alert("For the best experience please use a laptop or fesktop computer. The interactive viewer is not supported on mobile devices yet, but stay tuned for the mobile app.");
    }
    iframeload(userid :number,exbid:number){
      let logonUserId = 0;
    if(localStorage.getItem('userid') !== null){
      logonUserId = parseInt(localStorage.getItem('userid') || '');
    }
      this.setState({iframeurl:`${apiurlprefix}/`+
      VirtualUrl.virtualGalleryUrl +
      `?userid=${userid}&exhibitionid=${exbid}&loggeduserid=${logonUserId}`})
    }
  
    render() {
      const loading = this.state.loading;
      const exblist = this.state.exblist;
      const { windowWidth } = this.state; 
      return(

<> {this.state.loading?<Loader/>:
<div className="main">
        <div className="wrapper wrapper_inner">
        <div className="modal" id="myModal1" tabIndex={-1}>
          <div className="modal-dialog">
            <div className="modal-content">
              <iframe src={this.state.iframeurl} id="popup1" title="popup" className="modal-body" style={{width:'100%',minHeight:'550px'}}></iframe>
            <div className="modal-footer">
              <button type="button" className="btn btn-full-rounded" data-dismiss="modal">Close</button>
            </div>
          </div>
          </div>
          </div>
          <div className="image-container">
            {exblist.map((item:any)=>(
              <div className="image" key={item.virtualexhibition_id}>
                 {(!isMobile)
                              ?
              <button data-toggle="modal" data-target="#myModal1" data-backdrop="static" data-keyboard="false" className="button_slider" onClick={()=>this.iframeload(item.user_id,item.virtualexhibition_id)}>
                <img src={`${apiurlprefix}/${item.virtualexhibition_avatar}`} alt=""/>
              </button>
              :
              <button className="button_slider" onClick={()=>this.InfoMsg()}>
             <img src={`${apiurlprefix}/${item.virtualexhibition_avatar}`} alt=""/>
            </button>
                 }
          </div>
            ))}
          </div>
        </div>
      </div>
      }</>
      );
    }
  }
  
