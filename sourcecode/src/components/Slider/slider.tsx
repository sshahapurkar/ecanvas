import React, { Component } from 'react';
import ImageGallery from 'react-image-gallery';
import api from '../API/api';
import { apiurlprefix } from "../../apiconfig";
import Loader from '../container/loader';



// const images:any =[]; 
// const images = [
//   {
//     original: '/assets/images/art/ds1.jpg',
//     thumbnail: '/assets/images/art/ds1.jpg',
//   },
//   {
//     original: '/assets/images/art/SonavanePaintings/2c604774-3ec0-4c83-97e7-6b362b18a0c2.jpeg',
//     thumbnail: '/assets/images/art/SonavanePaintings/2c604774-3ec0-4c83-97e7-6b362b18a0c2.jpeg',
//   },
//   {
//     original: '/assets/images/art/SonavanePaintings/8fc54ead-e258-4700-8183-4b627960927e.jpeg',
//     thumbnail: '/assets/images/art/SonavanePaintings/8fc54ead-e258-4700-8183-4b627960927e.jpeg',
//   },
//   {
//     original: '/assets/images/art/SonavanePaintings/9d40fc8e-e30d-4a5b-876a-b4fe6e13f5ca.jpeg',
//     thumbnail: '/assets/images/art/SonavanePaintings/9d40fc8e-e30d-4a5b-876a-b4fe6e13f5ca.jpeg',
//   },
//   {
//     original: '/assets/images/art/SonavanePaintings/69ab6e17-9d1d-4dbc-aefd-92fc2889d0e9.jpeg',
//     thumbnail: '/assets/images/art/SonavanePaintings/69ab6e17-9d1d-4dbc-aefd-92fc2889d0e9.jpeg',
//   },
//   {
//     original: '/assets/images/art/SonavanePaintings/fa428053-be55-49ee-983c-f11c58b5847d.jpeg',
//     thumbnail: '/assets/images/art/SonavanePaintings/fa428053-be55-49ee-983c-f11c58b5847d.jpeg',
//   },
//   {
//     original: '/assets/images/art/SonavanePaintings/WhatsApp Image 2020-06-04 at 4.48.07 PM.jpeg',
//     thumbnail: '/assets/images/art/SonavanePaintings/WhatsApp Image 2020-06-04 at 4.48.07 PM.jpeg',
//   },
//   {
//     original: '/assets/images/art/SonavanePaintings/c8897994-1a96-40b8-a6a6-ebe153545f53.jpeg',
//     thumbnail: '/assets/images/art/SonavanePaintings/c8897994-1a96-40b8-a6a6-ebe153545f53.jpeg',
//   },
// ];

interface ISliderindex {
  loading:boolean,
  imageslist:any,
}
 
export class Sliderindex extends Component<{},ISliderindex> {
  constructor(props: {}) {
    super(props);
    this.state = {
      loading:true,
      imageslist:[],
    };
  }
  async onload(){
    let res = await api.getTopImages();
    let list = [];
    let count = 0;
    for(let i=0;i<res.length;i++){
      if(res[i].count!=null){
        count++;
        let sam = {
          original:`${apiurlprefix}/`+res[i].image_image,
          thumbnail:`${apiurlprefix}/`+res[i].image_image,
        }
        list.push(sam);
      }
      if(count==10){
        break;
      }
    }
    if(count<10){
      for(let i=0;i<res.length;i++){
      count++;
        let sam = {
          original:`${apiurlprefix}/`+res[i].image_image,
          thumbnail:`${apiurlprefix}/`+res[i].image_image,
        }
        list.push(sam);
        if(count == 10){
          break;
        }
      }
    }
    this.setState({imageslist:list, loading:false})
  }
  componentDidMount(){
    this.onload();
  }
  render() {
    const loading = this.state.loading;
    const images = this.state.imageslist;
    return(
      loading?<Loader/>:<ImageGallery items={images} />
    ) 
  }
}