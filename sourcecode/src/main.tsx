import React from "react";
import { HashRouter as Router,Switch, Route } from "react-router-dom";
import Home from "./components/container/home";
import Portfolio from "./components/container/portfolio";
import Comingsoon from "./components/container/Comingsoon";
import'bootstrap/dist/css/bootstrap.min.css';
import './style/css/style.css';
import './style/css/font4.css';
import './style/type/icons.css';
import Uploadart from "./components/container/uplaodart";
import Createexhibition from "./components/container/createexhibition";
import Payment from "./components/container/payment";
import Newexhibition from "./components/container/newexhibition";


const Main = () => {
  return (
    <main>
      <Switch>
        <Route exact path="/" component={Home} />
        <Route path="/portfolio" component={Portfolio} />
        <Route path="/aboutus" component={Comingsoon} />
        <Route path="/uploadart" component={Uploadart} />
        <Route path="/createexhibition" component={Createexhibition} />
        <Route path="/payment" component={Payment} />
        <Route path="/newexhibition" component={Newexhibition} />
      </Switch>
    </main>
  );
};

export default Main;
