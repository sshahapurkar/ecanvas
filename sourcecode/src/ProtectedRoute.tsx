import React from "react";
import { Route, Redirect,RouteProps } from "react-router-dom";

interface PrivateRouteProps extends RouteProps {
    component: any;
}
export const ProtectedRoute = (props: PrivateRouteProps) => {
    const { component: Component,...rest } = props;
  return (
    <Route
      {...rest}
      render={props => {
        if (localStorage.getItem('logonid')!==null) {
          return <Component {...props} />;
        } else {
          return (
            <Redirect
              to={{
                pathname: "/",
                state: {
                  from: props.location
                }
              }}
            />
          );
        }
      }}
    />
  );
};
