import {ConnectionOptions} from "typeorm";
export let dbOptions: ConnectionOptions = {
    type: "postgres",
    host: "localhost",
    port: 5432,
    username: "postgres",
    password: "postgres",
    database: "e_canvas",
    synchronize: true,
    logging: false,
    entities: [
    ],
    migrations: [
       "src/migration/**/*.ts"
    ],
    subscribers: [
       "src/subscriber/**/*.ts"
    ],
    "cli": {
       "entitiesDir": "src/entity",
       "migrationsDir": "src/migration",
       "subscribersDir": "src/subscriber"
    }
}

// import {ConnectionOptions} from "typeorm";
// export let dbOptions: ConnectionOptions = {
//     type: "postgres",
//     host: "localhost",
//     port: 5432,
//     username: "ecnvsdvapp",
//     password: "v5RcSwSvnv3qb",
//     database: "ecnvsappdb",
//     synchronize: true,
//     logging: false,
//     entities: [
//     ],
//     migrations: [
//        "src/migration/**/*.ts"
//     ],
//     subscribers: [
//        "src/subscriber/**/*.ts"
//     ],
//     "cli": {
//        "entitiesDir": "src/entity",
//        "migrationsDir": "src/migration",
//        "subscribersDir": "src/subscriber"
//     }
// }