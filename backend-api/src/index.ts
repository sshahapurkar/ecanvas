import "reflect-metadata";
import { createConnection, getRepository } from "typeorm";
import { Request, Response } from "express";
import * as express from "express";
import * as bodyParser from "body-parser";
import { AppRoutes } from "./routes";
import { dbOptions } from "./app";
import { baseurl } from "../baseurl";
import * as ExpressSession from 'express-session';
const cookieParser =  require('cookie-parser');


const app = express();
createConnection(dbOptions).then(async connection => {
    var cors = require('cors');

    app.use(cors());
    app.use(express.static(__dirname +'/public'))
    app.use(bodyParser.json({limit: '10mb', extended: true}));
    app.use(cookieParser());
    app.use(ExpressSession({secret: 'i-love-husky',resave: true,saveUninitialized: true}));
    AppRoutes.forEach(route => {
        app[route.method](route.path, (request: Request, response: Response, next: Function) => {
            route.action(request, response)
                .then(() => next)
                .catch(err => next(err));
        });
    });
    app.listen(8083);
    console.log("Express application is up and running on port 8083");

}).catch(error => console.log("TypeORM connection error: ", error));




