
import { checkUserExists, registration, imageCategory, uploadArt, logoutUpdate, tags, userId, checkEcanvasUserExists, getImages, searchImages, searchExb, exbList, createExbGall, exbtags, exbCategory, imageLike, userLikedImageid, checkEmailExists, imageViewCountCalculate, tagstop6, categorytop4, exbImages, exbImagesPosition, roomrentid, exbImagesPosition2, exbImages2, topImages, allExb, getUserInfo, updateUserInfo, getUserExb, exhibitionViewCountCalculate, imageDelete, exhibitionDelete, userLikedExhibitionid, exbLike} from "./action/insert";

export const AppRoutes = [
    {
        path: "/imgcat",
        method: "get",
        action: imageCategory
    },
    {
        path: "/exbcat",
        method: "get",
        action: exbCategory
    },
    {
        path: "/checkuser",
        method: "post",
        action: checkUserExists
    },
    {
        path: "/checkemail",
        method: "post",
        action: checkEmailExists
    },
    {
        path: "/logoutupdate",
        method: "post",
        action: logoutUpdate
    },
    {
        path: "/register",
        method: "post",
        action: registration
    },
    {
        path: "/uploadart",
        method: "post",
        action: uploadArt
    },
    {
        path: "/tags",
        method: "get",
        action: tags
    },
    {
        path: "/exbtags",
        method: "get",
        action: exbtags
    },
    {
        path: "/currentuser",
        method: "post",
        action: userId
    },
    {
        path: "/checkecanvasuser",
        method: "post",
        action: checkEcanvasUserExists
    },
    {
        path: "/getimages",
        method: "post",
        action: getImages
    },
    {
        path: "/searchimg",
        method: "post",
        action: searchImages
    },
    {
        path: "/searchexb",
        method: "post",
        action: searchExb
    },
    {
        path: "/exblist",
        method: "post",
        action: exbList
    },
    {
        path: "/exbgallery",
        method: "post",
        action: createExbGall
    },
    {
        path: "/imagecount",
        method: "post",
        action: imageViewCountCalculate
    },
    {
        path: "/imagelike",
        method: "post",
        action: imageLike
    },
    {
        path: "/userlikedimageid",
        method: "post",
        action: userLikedImageid
    },
    {
        path: "/toptag",
        method: "get",
        action: tagstop6
    },
    // {
    //     path: "/topcategory",
    //     method: "get",
    //     action: categorytop4
    // },
    // {
    //     path: "/exbimages2",
    //     method: "post",
    //     action: exbImages
    // },
    // {
    //     path: "/exbimagesposition",
    //     method: "post",
    //     action: exbImagesPosition
    // },
    {
        path: "/selectedroom",
        method: "post",
        action: roomrentid
    },
    {
        path: "/getuserinfo",
        method: "post",
        action: getUserInfo
    },
    {
        path: "/exbimagesposition/:galleryUpdate",
        method: "get",
        action: exbImagesPosition2
    },
    {
        path: "/exbimages/:userid/:exbid",
        method: "get",
        action: exbImages2
    },
    {
        path: "/topimages",
        method: "get",
        action: topImages
    },
    {
        path: "/getallexb",
        method: "get",
        action: allExb
    },
    {
        path: "/updateuserinfo",
        method: "post",
        action: updateUserInfo
    },
    {
        path: "/getuserexb",
        method: "post",
        action: getUserExb
    },
    {
        path: "/exhibitioncount",
        method: "post",
        action: exhibitionViewCountCalculate
    },
    {
        path: "/deleteimage",
        method: "post",
        action: imageDelete   
    },
    {
        path: "/deleteexhibition",
        method: "post",
        action: exhibitionDelete   
    },
    {
        path: "/exhibitionlike",
        method: "post",
        action: exbLike
    },
    {
        path: "/userlikedexhibitionid",
        method: "post",
        action: userLikedExhibitionid
    },
];