
// import { expect } from 'chai';
import { hello } from './action/hello';
import { GetAllAction, GetAddressAction } from './action/getall';
import {Request, Response} from "express";
import { Address } from './entity/address';
import {request} from "chai-http"
// if you used the '@types/mocha' method to install mocha type definitions, uncomment the following line
// import 'mocha';
var expect  = require('chai').expect;
var request = require('request');
describe('Hello function', () => {
  it('should return hello world', () => {
    const result = hello();
    expect(result).to.equal('Hello World!');
  });
 
});
describe('GET /address', function() {
it('returns a list of employees', function(done) {
request('http://localhost:8083/address' , function(error, response, body) {
expect(body).to.equal('[{"id":4,"city":"Trichendur","street":"sai street","state":"Tamilnadu"}]');
done();
})
});
});
describe('GETbyID /address', function() {
              it('returns a list of employees', function(done) {
              request('http://localhost:8083/address/4' , function(error, response, body) {
              expect(body).to.equal('{"id":4,"city":"Trichendur","street":"sai street","state":"Tamilnadu"}');
              done();
              })
              });
              });